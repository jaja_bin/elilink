package tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import pages.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class DeltaBooking {

    Logger Log = Logger.getLogger(DeltaBooking.class);
    WebDriver driver;
    DeltaMainPage objMainPage;
    DeltaTicketsSelectionPage objTicketsSelection;
    DeltaTripSummaryPage objTripSummary;
    DeltaPassengerInformationPage objPassengerInformation;
    DeltaReviewAndPurchasePage objCreditInformation;
    String[] passengerInformation = {"Zhanna", "Pasich", "Female", "March", "2", "1999",
            "Arthur", "Pasich", "Albania", "123456789",
            "Business", "Albania", "987654321", "eli@testmail.com"};

    @BeforeSuite
    public void startBrowser() {
        Log.info ("=========START TEST 3========");
        System.setProperty("webdriver.gecko.driver", "geckodriver-v0.16.1-win64\\geckodriver.exe");
        Log.info ("=> Start FireFox : delta.com");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("https://www.delta.com/");
        Log.info ("<= Start FireFox");
    }

    @BeforeSuite (dependsOnMethods = { "startBrowser" })
    public void setup() {
        Calendar dateDepart = new GregorianCalendar();
        dateDepart.add(Calendar.DAY_OF_YEAR, 5);
        Calendar dateReturn = new GregorianCalendar();
        dateReturn.add(Calendar.DAY_OF_YEAR, 11);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

        //objMainPage = new DeltaMainPage(driver, "JFK", "SVO", "05/19/2017", "05/25/2017");
        objMainPage = new DeltaMainPage(driver, "JFK", "SVO", dateFormat.format(dateDepart.getTime()), dateFormat.format(dateReturn.getTime()));

        objTicketsSelection = new DeltaTicketsSelectionPage(driver);
        objTripSummary = new DeltaTripSummaryPage(driver);
        objPassengerInformation = new DeltaPassengerInformationPage(driver, passengerInformation);
        objCreditInformation = new DeltaReviewAndPurchasePage(driver);
    }

    @Test (priority = 0)
    public void findFlight () {
        Log.info ("=> Start : findFlight");
        objMainPage.findFlights();
        Log.info ("<= Start : findFlight");
    }

    @Test (dependsOnMethods = { "findFlight" }, priority = 1)
    public void bookFlight () {
        Log.info ("=> Start : selectTickets");
        objTicketsSelection.selectOutboardTicket(); //1 ticket
        objTicketsSelection.selectReturnTicket(); //2 ticket
        objTripSummary.clickContinue();
        Log.info ("<= Start : selectTickets");
    }

    @Test (dependsOnMethods = { "bookFlight" }, priority = 2)
    public void enterPassengerInfo () {
        Log.info ("=> Start : enterPassengerInfo");
        objPassengerInformation.fillPassengerFields();
        Log.info ("<= Start : enterPassengerInfo");
    }

    @Test (dependsOnMethods = { "enterPassengerInfo" }, priority = 3)
    public void checkStateButtonCompletePurchase () {
        Log.info ("=> Start : checkStateButtonCompletePurchase");
        Log.info("      Complete Purchase button is active: " + objCreditInformation.isButtonContinueActive());
        Log.info ("<= Start : checkStateButtonCompletePurchase");
    }

/*    @AfterClass
    public void stop () {
        Log.info ("=> Stop FireFox");
        driver.quit();
        Log.info ("<= Stop FireFox");
    }
    */
}
