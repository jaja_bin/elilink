package tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.GmailAccoutPage;
import pages.GmailLoginPage;
import java.util.concurrent.TimeUnit;

public class GmailLogin {

    Logger Log = Logger.getLogger(GmailLogin.class);
    WebDriver driver;
    GmailLoginPage objLoginPage;
    GmailAccoutPage objAccountPage;
    By inbox = By.xpath("//a[contains(@title,'Inbox')]");
    By sentMail = By.xpath("//a[contains(@title,'Sent Mail')]");
    By spam = By.xpath("//a[contains(@title,'Spam')]");
    By btnMore = By.xpath("//span[contains(text(), 'More')]");

    @BeforeSuite
    public void setup() {
        Log.info ("=========START TEST 2========");
        System.setProperty("webdriver.gecko.driver", "geckodriver-v0.16.1-win64\\geckodriver.exe");
        Log.info ("=> Start FireFox : gmail");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://accounts.google.com/signin/v2/sl/pwd?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin&cid=1&navigationDirection=forward");
        Log.info ("<= Start FireFox");

        objLoginPage = new GmailLoginPage(driver, "eli2testmail@gmail.com", "eli12345678");
        objAccountPage = new GmailAccoutPage(driver);
    }

    @Test (priority = 0)
    public void validLoginToGmail () {
        Log.info ("=> Start loginToGmail");
        objLoginPage.loginToGmail();
        Log.info ("<= Start loginToGmail");
    }

    @Test (dependsOnMethods = { "validLoginToGmail" }, priority = 1)
    public void checkInboxPage() {
        Log.info ("=> Check Inbox page");
        objAccountPage.isPageExists(inbox, "Inbox");
        Log.info ("<= Check Inbox page");
    }

    @Test (dependsOnMethods = { "validLoginToGmail" }, priority = 2)
    public void checkSentMailPage() {
        Log.info ("=> Check Sent Mail page");
        objAccountPage.isPageExists(sentMail, "Sent Mail");
        Log.info ("<= Check Sent Mail page");
    }

    @Test (dependsOnMethods = { "checkSentMailPage" }, priority = 3)
    public void checkSpamPage() {
        Log.info ("=> Check Spam page");
        driver.findElement(btnMore).click();
        objAccountPage.isPageExists(spam, "Spam");
        Log.info ("<= Check Spam page");
    }

    @Test (dependsOnMethods = { "validLoginToGmail" }, priority = 4)
    public void findInboxEmails() {
        Log.info ("=> Find Inbox Emails");
        int countEmails = objAccountPage.getCountEmails("in:inbox bbb");
        Assert.assertEquals(countEmails, 3, "Actual doesn't egual Expected.");
        Log.info ("Found expected count of emails");
        objAccountPage.getListEmails(countEmails);
        Log.info ("<= Find Inbox Emails");
    }

    @Test (dependsOnMethods = { "validLoginToGmail" }, priority = 5)
    public void writeEmail() {
        Log.info ("=> Send Email");
        objAccountPage.sendEmail("eli2testmail@gmail.com", "test mail");
        Log.info ("<= Send Email");
    }

    @Test (dependsOnMethods = { "validLoginToGmail" }, priority = 6)
    public void signOut() throws InterruptedException {
        Log.info ("=> Logout");
        objAccountPage.logout();
        Log.info ("<= Logout");
    }

    @AfterClass
    public void stop () {
        Log.info ("=> Stop FireFox");
        //driver.close();
        driver.quit();
        Log.info ("<= Stop FireFox");
    }
}
