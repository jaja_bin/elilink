package tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;
import pages.TutByMainPage;
import pages.TutBySearchResults;

import java.util.concurrent.TimeUnit;

public class TutBySearch {

    Logger Log = Logger.getLogger(TutBySearch.class);
    WebDriver driver;
    TutByMainPage objMainPage;
    TutBySearchResults objSearchResults;
    String txtToSearch = "automated testing";
    String strResult = "automated-testing"; // will be found
    //String strResult = "Minsk Automated Testing Community"; // won't be found
    By result = By.xpath("//a[contains(text(), 'automated') and contains(text(), 'testing')]"); // will be found
    //By result = By.xpath("//a[contains(text(), 'Minsk') and contains(text(), 'Automated') and contains(text(), 'Testing') and contains(text(), 'Community')]"); // won't be found

    @BeforeClass
    public void setup() {
        Log.info ("=========START TEST 1========");
        System.setProperty("webdriver.gecko.driver", "geckodriver-v0.16.1-win64\\geckodriver.exe");
        Log.info ("=> Start FireFox : tut.by");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://tut.by");
        Log.info ("<= Start FireFox");
    }

    @Test (priority = 0)
    public void searchResult() {
        Log.info ("=> Start : searchString");
        objMainPage = new TutByMainPage(driver, txtToSearch);
        objMainPage.searchString();
        Log.info ("<= Start : searchString");
        Log.info ("=> Start : getCountResults");
        objSearchResults = new TutBySearchResults(driver, result);
        Log.info ("     getCountResults " + objSearchResults.getCountResults());
        System.out.println("FOUND " + txtToSearch + " items - " + objSearchResults.getCountResults());
        Log.info ("<= Start : getCountResults");
        Log.info ("=> Start : findResult");
        if (objSearchResults.findResult())
            Log.info ("     " + strResult + " found and clicked");
        else
            Log.error("ERROR:  " + strResult + " NOT FOUND");
        Log.info ("<= Start : findResult");
    }

    @AfterClass
    public void stop () {
        Log.info ("=> Stop FireFox");
        driver.quit();
        Log.info ("<= Stop FireFox");
    }

}
