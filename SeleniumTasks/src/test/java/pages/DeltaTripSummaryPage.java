package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class DeltaTripSummaryPage {

    WebDriver driver;
    String tripSummaryPage = "Trip Summary : Delta Air Lines";
    By btnContinue = By.xpath("//button[@id='tripSummarySubmitBtn']");

    public DeltaTripSummaryPage(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void clickContinue () {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.titleContains(tripSummaryPage));
        driver.findElement(btnContinue).click();
    }

}
