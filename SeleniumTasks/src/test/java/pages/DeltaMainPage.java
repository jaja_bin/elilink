package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.DeltaBooking;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class DeltaMainPage {

    WebDriver driver;
    Logger Log = Logger.getLogger(DeltaMainPage.class);
    String OriginCity;
    String DestinationCity;
    String DepartDate;
    String ReturnDate;
    By btnBookTrip = By.xpath(".//a[@id='navBookTrip']");
    String btnFlight = "book-air-content-trigger";
    String btnRoadTrip = "roundTripBtn";
    String inputOriginCity = "originCity";
    String inputDestinationCity = "destinationCity";
    String inputDepartDate = "departureDate";
    String inputReturnDate = "returnDate";
    String btnTravelDate = "exactDaysBtn";
    String btnShowPriceIn = "cashBtn";
    String btnFindFlights = "findFlightsSubmit";
    String ticketsSelectionPage = "Flight Results : Find & Book Airline Tickets : Delta Air Lines";

    public DeltaMainPage (WebDriver driver, String OriginCity, String DestinationCity, String DepartDate, String ReturnDate) {
        this.driver = driver;
        this.OriginCity = OriginCity;
        this.DestinationCity = DestinationCity;
        this.DepartDate = DepartDate;
        this.ReturnDate = ReturnDate;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void findFlights () {
        driver.findElement(btnBookTrip).click();
        JavascriptExecutor.class.cast(driver).executeScript("window.document.getElementById('" + btnFlight + "').click()");
        JavascriptExecutor.class.cast(driver).executeScript("window.document.getElementById('" + btnRoadTrip + "').click()");

        JavascriptExecutor.class.cast(driver).executeScript(" (document).getElementById('" + inputOriginCity + "').value = '" + OriginCity + "' ");
        JavascriptExecutor.class.cast(driver).executeScript(" (document).getElementById('" + inputDestinationCity + "').value = '" + DestinationCity + "' ");
        JavascriptExecutor.class.cast(driver).executeScript(" (document).getElementById('" + inputDepartDate + "').value = '" + DepartDate + "' ");
        JavascriptExecutor.class.cast(driver).executeScript(" (document).getElementById('" + inputReturnDate + "').value = '" + ReturnDate + "' ");

        JavascriptExecutor.class.cast(driver).executeScript("window.document.getElementById('" + btnTravelDate + "').click()");
        JavascriptExecutor.class.cast(driver).executeScript("window.document.getElementById('" + btnShowPriceIn + "').click()");

        JavascriptExecutor.class.cast(driver).executeScript("window.document.getElementById('" + btnFindFlights + "').click()");

        try {
            sleep(3000);
            if (driver.getTitle().contains(ticketsSelectionPage) == true)
                Log.info ("     Tickets found");
            else
                Log.error ("ERROR:      Tickets not found");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
