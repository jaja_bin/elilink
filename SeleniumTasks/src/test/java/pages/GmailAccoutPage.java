package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class GmailAccoutPage {

    WebDriver driver;
    Logger Log = Logger.getLogger(GmailAccoutPage.class);
    By inputSearch = By.cssSelector("#gbqfq");
    By btnSearch = By.cssSelector("#gbqfb");
    By actualResult = By.xpath("//div[@role='main']//div[5]//div//div//table/tbody//tr");
    By btnCompose = By.xpath("//div[contains(text(), 'COMPOSE')]");
    By inputTo = By.xpath("//textarea[@name='to']");
    By inputText = By.xpath(".//*[@role='textbox']");
    By btnSend = By.xpath("//div[contains(text(), 'Send')]");
    By btnAccout = By.xpath("//div[@role='banner']//*//div[4]//div//a[@role='button']");
    By btnLogout = By.xpath("//a[text()='Sign out']");

    public GmailAccoutPage (WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void isPageExists (By tempSection, String tempTitle) /*throws InterruptedException*/ {
        driver.findElement(tempSection).click();
        //Thread.sleep(3000);
        Wait<WebDriver> wait = new WebDriverWait(driver, 10, 1000);
        wait.until(ExpectedConditions.titleContains(tempTitle));
        if (driver.getTitle().contains(tempTitle) == true)
            Log.info ("Correct page opened");
        else {
            Log.info (driver.getTitle() + " :: " + tempTitle);
            Log.info ("ERROR    Wrong page opened");
        }
    }

    public int getCountEmails (String emailToFind) {
        WebElement searchEmails = driver.findElement(inputSearch);
        searchEmails.clear();
        searchEmails.sendKeys(emailToFind);
        driver.findElement(btnSearch).click();
        return driver.findElements(actualResult).size();
    }

    public void getListEmails (int count) {
        List<List<String>> listEmails = new ArrayList<List<String>>();
        List<String> singleEmail = new ArrayList<String>();

        for (int i = 1; i <= count; i++)
        {
            singleEmail.add(driver.findElement(By.xpath("//div[@role='main']//div[5]//div//div//table/tbody//tr[" + i + "]//td[4]//div[2]//span")).getText());
            singleEmail.add(driver.findElement(By.xpath("//div[@role='main']//div[5]//div//div//table/tbody//tr[" + i + "]//td[6]//div//div//div[2]//span[1]")).getText());
            singleEmail.add(driver.findElement(By.xpath("//div[@role='main']//div[5]//div//div//table/tbody//tr[" + i + "]//td[6]//div//div//div[2]//span[2]")).getText());
            listEmails.add(new ArrayList<String>(singleEmail));
            singleEmail.clear();
        }

        for (int i = 0; i < listEmails.size(); i++)
            Log.info (listEmails.get(i));
    }

    public void sendEmail (String emailTo, String emailText) {
        driver.findElement(btnCompose).click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(driver.findElement(inputTo)));
        driver.findElement(inputTo).sendKeys(emailTo);
        driver.findElement(inputTo).submit();
        driver.findElement(inputText).click();
        driver.findElement(inputText).sendKeys(emailText);
        driver.findElement(btnSend).click();
    }

    public void logout () throws InterruptedException {
        sleep(3000);
        driver.findElement(btnAccout).click();
        driver.findElement(btnLogout).click();
    }

}
