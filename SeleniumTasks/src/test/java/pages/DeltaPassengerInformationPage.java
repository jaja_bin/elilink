package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.DeltaBooking;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class DeltaPassengerInformationPage {
    WebDriver driver;
    Logger Log = Logger.getLogger(DeltaBooking.class);
    String passengerInformationPage = "Passenger Information Page : Delta Air Lines";
    String[] passengerInfo = new String[14]; //{"firstName", "lastName", "gender", "month", "day", "year",
                                            //  "emgcFirstName", "emgcLastName", "emgcCountry", "emgcPhoneNumber",
                                            //  "infoDeviceType", "infoCountry", "infoPhoneNumber", "email"};
    By passengerFirstName = By.xpath("//input[@id='firstName0']");
    By passengerLastName = By.xpath("//input[@id='lastName0']");
    By passengerGenderBtn = By.xpath("//span[@id='gender0-button']");
    By passengerGenderMenu = By.xpath("//ul[@id='gender0-menu']");
    By passengerGender;// = By.xpath("//ul[@id='gender0-menu']//li[contains(text(), '" +  passengerInfo[2] + "']");
    By passengerMonthBtn = By.xpath("//span[@id='month0-button']");
    By passengerMonthMenu = By.xpath("//ul[@id='month0-menu']");
    By passengerMonth;// = By.xpath("//ul[@id='month0-menu']//li[contains(text(), '" +  passengerInfo[3] + "']");
    By passengerDayBtn = By.xpath("//span[@id='day0-button']");
    By passengerDayMenu = By.xpath("//ul[@id='day0-menu']");
    By passengerDay;// = By.xpath("//ul[@id='day0-menu']//li[contains(text(), '" +  passengerInfo[4] + "']");
    By passengerYearBtn = By.xpath("//span[@id='year0-button']");
    By passengerYearMenu = By.xpath("//ul[@id='year0-menu']");
    By passengerYear;// = By.xpath("//ul[@id='year0-menu']//li[contains(text(), '" +  passengerInfo[5] + "']");
    By emergencyFirstName = By.xpath("//input[@id='emgcFirstName_0']");
    By emergencyLastName = By.xpath("//input[@id='emgcLastName_0']");
    By emergencyCountryBtn = By.xpath("//span[@id='emgcCountry_0-button']");
    By emergencyCountryMenu = By.xpath("//ul[@id='emgcCountry_0-menu']");
    By emergencyCountry;// = By.xpath("//ul[@id='emgcCountry_0-menu']//li[contains(text(), '" +  passengerInfo[8] + "']");
    By emergencyPhone = By.xpath("//input[@id='emgcPhoneNumber_0']");
    By infoDeviceTypeBtn = By.xpath("//span[@id='deviceType-button']");
    By infoDeviceTypeMenu = By.xpath("//ul[@id='deviceType-menu']");
    By infoDeviceType;// = By.xpath("//ul[@id='deviceType-menu']//li[contains(text(), '" +  passengerInfo[10] + "']");
    By infoCountryBtn = By.xpath("//span[@id='countryCode0-button']");
    By infoCountryMenu = By.xpath("//ul[@id='countryCode0-menu']");
    By infoCountry;// = By.xpath("//ul[@id='countryCode0-menu']//li[contains(text(), '" +  passengerInfo[11] + "']");
    By infoPhone = By.xpath("//input[@id='telephoneNumber0']");
    By infoEmail = By.xpath("//input[@id='email']");
    By infoConfirmEmail = By.xpath("//input[@id='reEmail']");
    By btnContinue = By.xpath("//button[@id='paxReviewPurchaseBtn']");
    String ReviewAndPurchasePage = "Review and Purchase : Delta Air Lines";

    public DeltaPassengerInformationPage(WebDriver driver, String[] passengerInfo) {
        this.driver = driver;
        this.passengerInfo = passengerInfo;
        this.passengerGender = By.xpath("//ul[@id='gender0-menu']//li[contains(text(), '" +  passengerInfo[2] + "') and contains(@class, 'ui-state-focus')]");
        this.passengerMonth = By.xpath("//ul[@id='month0-menu']//li[contains(text(), '" +  passengerInfo[3] + "') and contains(@class, 'ui-state-focus')]");
        this.passengerDay = By.xpath("//ul[@id='day0-menu']//li[contains(text(), '" +  passengerInfo[4] + "') and contains(@class, 'ui-state-focus')]");
        this.passengerYear = By.xpath("//ul[@id='year0-menu']//li[contains(text(), '" +  passengerInfo[5] + "') and contains(@class, 'ui-state-focus')]");
        this.emergencyCountry = By.xpath("//ul[@id='emgcCountry_0-menu']//li[contains(text(), '" +  passengerInfo[8] + "') and contains(@class, 'ui-state-focus')]");
        this.infoDeviceType = By.xpath("//ul[@id='deviceType-menu']//li[contains(text(), '" +  passengerInfo[10] + "') and contains(@class, 'ui-state-focus')]");
        this.infoCountry = By.xpath("//ul[@id='countryCode0-menu']//li[contains(text(), '" +  passengerInfo[11] + "') and contains(@class, 'ui-state-focus')]");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void fillPassengerFields () {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.titleContains(passengerInformationPage));

        //Passenger
        driver.findElement(passengerFirstName).sendKeys(passengerInfo[0]);
        driver.findElement(passengerLastName).sendKeys(passengerInfo[1]);
        driver.findElement(passengerGenderBtn).click();
        scroll(passengerGenderMenu, passengerGender);
        driver.findElement(passengerMonthBtn).click();
        scroll(passengerMonthMenu, passengerMonth);
        driver.findElement(passengerDayBtn).click();
        scroll(passengerDayMenu, passengerDay);
        driver.findElement(passengerYearBtn).click();
        scroll(passengerYearMenu, passengerYear);

        //Emergency Contact Information
        driver.findElement(emergencyFirstName).sendKeys(passengerInfo[6]);
        driver.findElement(emergencyLastName).sendKeys(passengerInfo[7]);
        driver.findElement(emergencyCountryBtn).click();
        scroll(emergencyCountryMenu, emergencyCountry);
        driver.findElement(emergencyPhone).sendKeys(passengerInfo[9]);

        //Contact Information
        driver.findElement(infoDeviceTypeBtn).click();
        scroll(infoDeviceTypeMenu, infoDeviceType);
        driver.findElement(infoCountryBtn).click();
        scroll(infoCountryMenu, infoCountry);
        driver.findElement(infoPhone).sendKeys(passengerInfo[12]);
        driver.findElement(infoEmail).sendKeys(passengerInfo[13]);
        driver.findElement(infoConfirmEmail).sendKeys(passengerInfo[13]);

        driver.findElement(btnContinue).click();

        try {
            sleep(3000);
            if (driver.getTitle().contains(ReviewAndPurchasePage) == true)
                Log.info ("     All fields filled");
            else
                Log.error ("ERROR:      Fill all fields");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void scroll (By tempMenu, By tempElement) {
        while (isElementExists(tempElement) == false)
            driver.findElement(tempMenu).sendKeys(Keys.ARROW_DOWN);
        driver.findElement(tempElement).click();
    }

    public boolean isElementExists (By tempElement) {
        try {
            driver.findElement(tempElement);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

   /* public void selectItemInDropdown (String tempMenu, String tempElement) {
        Select select = new Select(driver.findElement(By.xpath(tempMenu)));
        select.selectByVisibleText(tempElement);

        WebElement select = driver.findElement(By.xpath(tempMenu));
        List<WebElement> options = select.findElements(By.tagName("option"));
        //List<WebElement> options = driver.findElements(By.xpath(".//*[@id='gender0']/option"));
        for (WebElement option : options) {
            System.out.println(option.getText());
            if (option.getText().equals(tempElement)) {
                option.click();
                break;
            }
        }
    }*/

}
