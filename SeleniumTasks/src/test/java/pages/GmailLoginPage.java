package pages;

//eli2testmail@gmail.com
//eli12345678

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class GmailLoginPage {

    WebDriver driver;
    String userEmail;
    String userPassword;

    By refDifAccount = By.id("account-chooser-link");
    By refAddNewAccount = By.id("account-chooser-add-account");
    By inputEmail = By.xpath("//.[@id='Email' or @id='identifierId']");
    By inputPass = By.xpath("//*[@name='Passwd' or @name='password']");
    By btnNext = By.xpath("//div[@id='identifierNext' or @id='passwordNext']");

    public GmailLoginPage (WebDriver driver, String userEmail, String userPassword) {
        this.driver = driver;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void loginToGmail () {
        if (isElementExists(inputEmail) == true)
            login();
        else {
            driver.findElement(refDifAccount).click();
            driver.findElement(refAddNewAccount).click();
            login();
        }
    }

    public void login () {
        //enter EMail
        WebElement login = driver.findElement(inputEmail);
        login.sendKeys(userEmail);
        login.submit();
        if (isElementExists(btnNext) == true)
            driver.findElement(btnNext).click();
        WebElement password = driver.findElement(inputPass);

        //enter password
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(password));
        password.sendKeys(userPassword);
        password.submit();
        if (isElementExists(btnNext) == true)
            driver.findElement(btnNext).click();
    }

    public boolean isElementExists(By tempItem) {
        try {
            driver.findElement(tempItem);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

}
