package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class DeltaReviewAndPurchasePage {

    WebDriver driver;
    String ReviewAndPurchasePage = "Review and Purchase : Delta Air Lines";
    By btnCompletePurchase = By.xpath("//button[@id='continue_button']");

    public DeltaReviewAndPurchasePage(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public boolean isButtonContinueActive () {
        if (driver.findElement(btnCompletePurchase).isEnabled())
            return true;
        else
            return false;
    }

}
