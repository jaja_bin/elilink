package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class DeltaTicketsSelectionPage {

    WebDriver driver;
    String ticketsSelectionPage = "Flight Results : Find & Book Airline Tickets : Delta Air Lines";
    By ticket = By.xpath("//*[@id='0_0_0']");
    By blockMessage = By.xpath("//div[@class='blockUI blockMsg blockPage']");

    public DeltaTicketsSelectionPage (WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void selectOutboardTicket () {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.titleContains(ticketsSelectionPage));
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(ticket));
        driver.findElement(ticket).click();
    }

    public void selectReturnTicket () {
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOfElementLocated(blockMessage));
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(ticket));
        driver.findElement(ticket).click();
    }
}
