package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TutByMainPage {

    WebDriver driver;
    By inputSearch = By.id("search_from_str");
    String txtToSearch;

    public TutByMainPage (WebDriver driver, String txtToSearch) {
        this.driver = driver;
        this.txtToSearch = txtToSearch;
    }

    public void searchString () {
        WebElement search = driver.findElement(inputSearch);
        search.sendKeys(txtToSearch);
        search.submit();
    }

}
