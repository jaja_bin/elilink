package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import java.util.List;

public class TutBySearchResults {

    WebDriver driver;
    By countResults = By.xpath("//ol[@class='b-results-list']/li");
    By result;

    public TutBySearchResults (WebDriver driver, By result) {
        this.driver = driver;
        this.result = result;
    }

    public int getCountResults () {
        List<WebElement> count = driver.findElements(countResults);
        return count.size();
    }

    public boolean findResult () {
        try {
            driver.findElement(result);
            driver.findElement(result).click();
            return true;
        } catch (WebDriverException e) {
            return false;
        }
    }
}
