package FTP;

import org.apache.log4j.Logger;

import java.io.IOException;

public class FTPMain {

    ConnectToFTP objFTP;
    Logger Log = Logger.getLogger(FTPMain.class);


    public void workWithFTP () throws IOException {
        Log.info("===========FTPMain==========");
        objFTP = new ConnectToFTP();
        objFTP.connect("ftp.byfly.by", 21, "anonymous", "anonymous");
        Log.info("Connection established");
        Log.info("Current directory is " + objFTP.pwd());
        Log.info("Go to blog directory");
        objFTP.cwd("blog");
        Log.info("Current directory is " + objFTP.pwd());
        //objFTP.pasv();
        //Log.info("List of all directories ");
        //objFTP.list();
        Log.info("Create a new folder");
        if (objFTP.mkd("elitest")) {
            Log.info("Folder created. Remove");
            objFTP.rmd("elitest");
        } else {
            Log.error("Folder not created");
        }
        objFTP.disconnect();
        Log.info("Disconnected");

    }

}
