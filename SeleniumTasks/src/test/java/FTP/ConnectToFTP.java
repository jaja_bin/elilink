package FTP;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;

public class ConnectToFTP {


    public ConnectToFTP () {}

    //SimpleFTP is a simple package that implements a Java FTPMain client. With
    //SimpleFTP, you can connect to an FTPMain server and upload multiple files.

    Logger Log = Logger.getLogger(ConnectToFTP.class);
    private Socket socket = null;
    private BufferedReader reader = null;
    private BufferedWriter writer = null;
    private static boolean DEBUG = false;

    //Connects to an FTPMain server and logs in with the supplied username and password.

    public void connect(String host, int port, String user, String pass) throws IOException {
        if (socket != null) {
            throw new IOException("FTPMain is already connected. Disconnect first.");
        }
        socket = new Socket(host, port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream()));

        String response = readLine();
        if (!response.startsWith("220 ")) {
            throw new IOException(
                    "FTPMain received an unknown response when connecting to the FTPMain server: "
                            + response);
        }

        sendLine("USER " + user);

        response = readLine();
        if (!response.startsWith("331 ")) {
            throw new IOException(
                    "FTPMain received an unknown response after sending the user: "
                            + response);
        }

        sendLine("PASS " + pass);

        response = readLine();
        if (!response.startsWith("230 ")) {
            throw new IOException(
                    "FTPMain was unable to log in with the supplied password: "
                            + response);
        }
        // Now logged in.
    }

    //Disconnects from the FTPMain server.
    public void disconnect() throws IOException {
        try {
            sendLine("QUIT");
        } finally {
            socket = null;
        }
    }

    //Returns the working directory of the FTPMain server it is connected to
    public String pwd() throws IOException {
        sendLine("PWD");
        String dir = null;
        String response = readLine();
        if (response.startsWith("257 ")) {
            Log.info("PWD completed");
            int firstQuote = response.indexOf('\"');
            int secondQuote = response.indexOf('\"', firstQuote + 1);
            if (secondQuote > 0) {
                dir = response.substring(firstQuote + 1, secondQuote);
            }
        } else {
            Log.error("PWD not completed");
            Log.error("FTPMain Server answer : " + response);
        }
        return dir;
    }

    //Returns the working directories of the FTPMain server it is connected to
    public void list() throws IOException {
        sendLine("LS -l");
        List<String> directories = null;
        String response = readLine();
        //if (response.startsWith("257 "))
        // for (String dir : directories) {
        //     System.out.println("directory : " + dir);
        // }
        if (response.startsWith("226 "))
            Log.info("LIST completed");
        else {
            Log.error("LIST not completed");
            Log.error("FTPMain Server answer : " + response);
        }
    }

    //Sets passive mode
    public void pasv() throws  IOException {
        sendLine("PASV");
        String response = readLine();
        if (response.startsWith("227 "))
            Log.info("PASV completed");
        else {
            Log.error("PASV not completed");
            Log.error("FTPMain Server answer : " + response);
        }
    }

    //Changes the working directory (like cd). Returns true if successful.
    public void cwd(String dir) throws IOException {
        sendLine("CWD " + dir);
        String response = readLine();
        if (response.startsWith("250 "))
            Log.info("CWD completed");
        else {
            Log.error("CWD not completed");
            Log.error("FTPMain Server answer : " + response);
        }
    }

    //Creates a new working directory
    public boolean mkd(String dir) throws IOException{
        sendLine("MKD " + dir);
        String response = readLine();
        if (response.startsWith("257 ")) {
            Log.info("MKD completed");
            return true;
        } else {
            Log.error("MKD not completed");
            Log.error("FTPMain Server answer : " + response);
            return false;
        }
    }

    //Removes the working directory
    public void rmd(String dir) throws IOException{
        sendLine("RMD " + dir);
        String response = readLine();
        if (response.startsWith("250 "))
            Log.info("RMD completed");
        else {
            Log.error("MKD not completed");
            Log.error("FTPMain Server answer : " + response);
        }
    }

    //Sends a raw command to the FTPMain server.
    private void sendLine(String line) throws IOException {
        if (socket == null) {
            throw new IOException("FTPMain is not connected.");
        }
        try {
            writer.write(line + "\r\n");
            writer.flush();
            if (DEBUG) {
                System.out.println("> " + line);
            }
        } catch (IOException e) {
            socket = null;
            throw e;
        }
    }

    private String readLine() throws IOException {
        String line = reader.readLine();
        if (DEBUG) {
            System.out.println("< " + line);
        }
        return line;
    }


}
